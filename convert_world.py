"""
In versions up to 24.05.21.x the world was saved in seperate files in the world/ directory.
In the new version the world is saved in a sqlite3 database.
If you want to upgrade your server installation, you can run this script to convert the world files to the new database.
This will also remove the old world files, so maybe make a backup first.

-- requirements --
pip install progressbar2==4.2.0

-- Technical details --
The world/ directory contains .chunk files with the patern world/[x,y].chunk.
These need to be added to the sqlite3 database `world.db` in the table `chunks`.
Key: [x,y], Value: .chunk text file content.
It are a lot of files, so this script is optimized to be fast.
The table is already created with "CREATE TABLE chunks (chunk_location TEXT PRIMARY KEY, chunk TEXT)".
"""

import sqlite3
import os
from progressbar import ProgressBar

db = sqlite3.connect('world.db')
cursor = db.cursor()

files = [f for f in os.listdir('world/') if f.endswith('.chunk')]

bar = ProgressBar()
try:
    for i, file in enumerate(bar(files)):
        with open('world/' + file, 'r') as f:
            chunk_location = file.split('.')[0]
            chunk_content = f.read()
            # check if chunk_location already exists
            cursor.execute('SELECT chunk_location FROM chunks WHERE chunk_location = ?', (chunk_location,))
            if not cursor.fetchone():
                cursor.execute('INSERT INTO chunks (chunk_location, chunk) VALUES (?, ?)', (chunk_location, chunk_content))
                if i % 1000 == 0:
                    db.commit()
            os.remove('world/' + file)
except Exception as e:
    print('Error:', e)
finally:
    db.commit()
    db.close()
    print('Done.')