var express = require('express');
var socket = require('socket.io');
const { exec } = require('child_process');
var world_module = require('./world_module')

// Get the date of the last commit
let version = 'unknown';
exec('git log -1 --format=%cd', (err, stdout, stderr) => {
    if (err) {
        console.error(err);
        return;
    }
    // Format the date
    let [weekday, month, day, time, year, timezone] = stdout.split(' ');
    const [hour, minute, second] = time.split(':');
    month = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"].indexOf(month.toLowerCase())+1;
    version = `${year.slice(2)}.${month}.${day}.${hour}`;
});


function mod (n, m) {
    const remain = n % m;
    return Math.floor(remain >= 0 ? remain : remain + m);
}

class Player {
    constructor(x, y) {
        this.location = [x,y];
        this.velocity = [0,0];
    }
}
var players = {};


var app = express();
var server = app.listen(process.env.PORT || 5000, function () {
    console.log('App avaiable on http://localhost:5000');
})
var io = socket(server);

app.use(express.static('public'))
app.use('/scripts', express.static(__dirname + '/scripts'))
app.use('/textures', express.static(__dirname + '/textures'))

app.get('/', async (request, response) => {
    response.sendFile(__dirname + '/public/game.html');
});

app.get('/version', async (request, response) => {
    response.send(version);
});

io.on("connection", function (sock) {
    console.log('a user connected with id', sock.id);
    sock.join('global_room');
    players[sock.id] = new Player(0, 4);
    sock.emit('set_players', players);
    sock.emit('set_user_id', sock.id);
    sock.to('global_room').emit('update_players', {[sock.id]: players[sock.id]});

    sock.on('disconnect', () => {
        delete players[sock.id];
        sock.to('global_room').emit('remove_player', sock.id);
        console.log(sock.id, 'disconnected.')
    });

    sock.on('update_my_player', player => {
        players[sock.id] = player;
        sock.to('global_room').emit('update_players', {[sock.id]: players[sock.id]});
    });

    sock.on('get_chunks', chunk_locations => {
        // let chunks_zipped = []
        // for (const chunk_location of chunk_locations) {
        //     chunks_zipped.push([chunk_location, world_module.get_chunk(chunk_location)]);
        // }
        // sock.emit('set_chunks', chunks_zipped);
        for (const chunk_location of chunk_locations) {
            world_module.get_chunk(chunk_location).then(chunk => {
                sock.emit('set_chunks', [[chunk_location, chunk]]);
            });
        }
    });

    sock.on('set_block', data => {
        sock.to('global_room').emit('set_block', data);
        const block_location = data[0];
        const block = data[1];
        world_module.set_block(block_location, block);
    });
})
