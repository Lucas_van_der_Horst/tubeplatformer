const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

// Setting up the database
const db = new sqlite3.Database('world.db', (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the world database.');
});
process.on('SIGINT', () => {
    db.close();
    console.log('Closed the world database.');
    process.exit();
});
// If the database is empty then create the table
db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='chunks'", (err, row) => {
    if (err) {
        console.error(err.message);
    }
    if (!row) {
        db.run("CREATE TABLE chunks (chunk_location TEXT PRIMARY KEY, chunk TEXT)", (err) => {
            if (err) {
                console.error(err.message);
            }
        });
    }
});

// If the "world/" folder exists with any "*.chunk" files inside. 
if (fs.existsSync('world')) {
    console.log("Found an old world folder. Please use `convert_world.py` to convert the world folder to the database.");
};

function mod (n, m) {
    const remain = n % m;
    return Math.floor(remain >= 0 ? remain : remain + m);
}
const chunk_size = 8;

function float_location_to_chunk_location(float_location) {
    return float_location.map((_, dim) => Math.floor(float_location[dim]/chunk_size));
}

const random_list = [469, 936, 125, 842, 357, 528, 821, 37, 523, 455, 885, 998, 960, 179, 584, 910, 993, 952, 482, 270, 14, 100, 200, 605, 216, 66, 533, 441, 423, 630, 416, 999, 47, 860, 446, 395, 825, 513, 971, 270, 510, 520, 804, 354, 169, 845, 952, 872, 526, 952, 193, 522, 228, 768, 171, 549, 106, 908, 299, 148, 112, 792, 332, 907, 942, 765, 118, 542, 968, 133, 238, 927, 797, 949, 690, 172, 430, 180, 154, 99, 535, 487, 993, 601, 821, 584, 241, 101, 325, 101, 965, 101, 801, 449, 607, 204, 117, 967, 41, 902, 254, 378, 22, 75, 62, 341, 638, 728, 370, 425, 846, 485, 114, 130, 517, 846, 474, 330, 159, 287, 127, 375, 409, 433, 678, 528, 952, 39, 618, 377, 455, 28, 172, 891, 204, 400, 127, 621, 120, 17, 606, 871, 116, 581, 519, 985, 20, 130, 287, 179, 89, 712, 55, 674, 684, 330, 556, 22, 260, 606, 70, 722, 190, 186, 158, 471, 5, 337, 644, 0, 958, 848, 186, 578, 878, 159, 162, 659, 7, 185, 895, 447, 832, 357, 741, 1, 778, 121, 650, 704, 10, 53, 539, 206, 87, 786, 433, 587, 886, 85, 347, 799, 479, 22, 462, 847, 500, 758, 111, 797, 442, 570, 801, 887, 403, 294, 601, 295, 44, 608, 553, 576, 864, 378, 640, 238, 924, 771, 288, 534, 238, 540, 902, 556, 861, 916, 784, 751, 525, 853, 910, 139, 393, 843, 366, 50, 147, 15, 201, 962, 125, 626, 495, 431, 44, 309, 183, 177, 175, 568, 956, 527, 544, 306, 162, 454, 978, 401, 742, 162, 342, 321, 364, 362, 593, 264, 808, 677, 710, 765, 671, 132, 471, 463, 920, 873, 355, 670, 117, 715, 560, 978, 99, 169, 684, 666, 982, 787, 728, 177, 915, 306, 859, 565, 805, 645, 417, 582, 108, 10, 959, 535, 761, 378, 47, 207, 600, 788, 267, 862, 45, 89, 602, 808, 345, 412, 933, 225, 804, 771, 182, 700, 731, 846, 59, 788, 160, 135, 3, 112, 987, 198, 59, 871, 157, 758, 159, 990, 80, 643, 45, 215, 222, 711, 233, 880, 130, 396, 348, 519, 206, 774, 451, 96, 179, 469, 682, 204, 353, 737, 70, 819, 325, 740, 266, 473, 576, 451, 880, 281, 825, 445, 945, 245, 956, 178, 560, 776, 171, 987, 95, 401, 886, 144, 197, 862, 284, 531, 934, 649, 583, 504, 412, 777, 600, 728, 166, 768, 103, 399, 131, 976, 969, 400, 139, 448, 507, 436, 77, 352, 703, 188, 428, 504, 932, 935, 406, 111, 671, 256, 801, 233, 525, 450, 803, 311, 325, 91, 404, 847, 633, 828, 508, 550, 415, 126, 929, 502, 721, 685, 925, 725, 93, 755, 677, 23, 212, 743, 782, 151, 540, 967, 688, 859, 71, 962, 781, 305, 996, 4, 244, 928, 564, 36, 657, 643, 22, 670, 102, 563, 768, 951, 226, 273, 503, 594, 709, 287, 785, 469, 295, 723, 792, 733, 575, 887, 616, 234, 371, 68];

function random_from_two_inputs(input1, input2) {
    return random_list[(random_list[mod(input1*2,500)] + random_list[mod(input2*2,500)]) % 500];
}

function any_matrix(matrix_1, matrix_2) {
    for (let y=0; y<matrix_1.length; y++) {
        for (let x=0; x<matrix_1[0].length; x++) {
            matrix_1[y][x] = matrix_1[y][x] | matrix_2[y][x];
        }
    }
    return matrix_1;
}

function generate_chunk(chunk_location) {
    let new_chunk = [
        [1,0,0,0,0,0,0,1],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [1,1,0,0,0,0,1,1],
        [1,1,0,0,0,0,1,1],
    ]
    if (random_from_two_inputs(chunk_location[0]+0.5, chunk_location[1]) % 2 === 1) {
        new_chunk = any_matrix(new_chunk, [
            [0,1,1,1,1,1,1,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
        ])
    }
    if (random_from_two_inputs(chunk_location[0]+1, chunk_location[1]+0.5) % 2 === 1) {
        new_chunk = any_matrix(new_chunk, [
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,1],
            [0,0,0,0,0,0,1,0],
            [0,0,0,0,0,0,0,0],
        ])
    }
    if (random_from_two_inputs(chunk_location[0]+0.5, chunk_location[1]+1) % 2 === 1) {
        new_chunk = any_matrix(new_chunk, [
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,1,1,1,1,1,1,0],
            [0,0,1,1,1,1,0,0],
        ])
    }
    if (random_from_two_inputs(chunk_location[0],chunk_location[1]+0.5) % 2 === 1) {
        new_chunk = any_matrix(new_chunk, [
            [1,1,0,0,0,0,0,0],
            [1,1,0,0,0,0,0,0],
            [1,1,0,0,0,0,0,0],
            [1,1,0,0,0,0,0,0],
            [1,1,0,0,0,0,0,0],
            [1,1,0,0,0,0,0,0],
            [0,1,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
        ])
    }
    if (new_chunk[0][2] && new_chunk[2][7] && new_chunk[7][2] && new_chunk[2][0]) {
        new_chunk = any_matrix(new_chunk, [
            [0,0,0,0,0,0,0,0],
            [0,0,1,1,1,1,0,0],
            [0,0,1,1,1,1,0,0],
            [0,0,1,1,1,1,0,0],
            [0,0,1,1,1,1,0,0],
            [0,0,1,1,1,1,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
        ])
    }
    return new_chunk;
}

/**
 * Get a chunk from the database or generate a new one if it doesn't exist.
 * @param {number[]} chunk_location - The location of the chunk.
 * @returns {Promise<number[][]>} The chunk.
 */
function get_chunk(chunk_location) {
    return new Promise((resolve, reject) => {
        const sql = 'SELECT chunk FROM chunks WHERE chunk_location = ?';
        db.get(sql, [JSON.stringify(chunk_location)], (err, row) => {
            if (err) {
                reject(err);
            }
            let chunk;
            if (row) {
                chunk = JSON.parse(row.chunk);
            } else {
                chunk = generate_chunk(chunk_location);
                const sql = 'INSERT INTO chunks (chunk_location, chunk) VALUES (?, ?)';
                db.run(sql, [JSON.stringify(chunk_location), JSON.stringify(chunk)], (err) => {
                    if (err) {
                        reject(err);
                    }
                });
            }
            resolve(chunk);
        });
    });
}

/**
 * Set a block in the database. Done asynchronously.
 * @param {number[]} block_location - The location of the block.
 * @param {number} block - The block to set.
 * @returns {undefined}
 */
function set_block(block_location, block) {
    const chunk_location = float_location_to_chunk_location(block_location);
    get_chunk(chunk_location).then(chunk => {
        if (!chunk) {
            console.log(`Tried to set a block in a non-existent chunk. ${block_location} ${block}`);
            return;
        }
        chunk[mod(block_location[1], chunk_size)][mod(block_location[0], chunk_size)] = block;
        const sql = 'UPDATE chunks SET chunk = ? WHERE chunk_location = ?';
        db.run(sql, [JSON.stringify(chunk), JSON.stringify(chunk_location)], (err) => {
            if (err) {
                console.error(err.message);
            }
        });
    });
}

module.exports = {get_chunk, set_block};