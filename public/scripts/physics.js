import { euclidean_distance_squared, neg_pos } from "./math_utils.js";

window.speed_modifier = 10;
window.friction = 0.2;
window.gravity = 70;
window.coyote_time = 100;
window.jump_power = 22;
window.walljump_cooldown = 500;
window.walljump_power = 22;
window.walljump_horiontal_power = 15;
window.horizontal_jump_boost = 0.63;
window.stop_jump_drag = 1000;

export function physics_step(delta_t, players, controller, socket, my_id) {
    let my_player = players[my_id];
    controller_based_adjustments(my_player, controller, delta_t);
    for (let player of Object.values(players)) {
        update_player(player);
    }

    // Update UI and send player location to server
    document.getElementById("my_player_location_text").innerText = 'player location: (' + my_player.location[0].toFixed(2) + ', ' + my_player.location[1].toFixed(2) + ')'
    if (euclidean_distance_squared(my_player.last_send_location, my_player.location) > 0.01) {
        // if location change is big enough than send to server
        socket.emit('update_my_player', {
            location: my_player.location,
            velocity: my_player.velocity
        });
        my_player.last_send_location = [...my_player.location];  // copy by value
    }
}

function controller_based_adjustments(player, controller, delta_t) {
    // My player specific physics adjustments based on controller input
    if (-neg_pos(controller.horizontal_move) !== player.last_walljump_key) {
        player.last_walljump_key = undefined;
    }
    if (((performance.now() - player.last_walljump) || Infinity) > window.walljump_cooldown || player.last_walljump_key === undefined) {
        player.velocity[0] += controller.horizontal_move * window.speed_modifier * delta_t;
    }
    if ((Math.abs(player.last_ground_touch - controller.last_jump_time) || Infinity) < window.coyote_time && !player.in_jump) {
        player.velocity[1] -= window.jump_power + Math.abs(player.velocity[0]) * window.horizontal_jump_boost;
        //controller.last_jump_time = undefined;
        player.in_jump = true;
    }
    if (player.wallslide && (Math.abs(performance.now() - controller.last_jump_time) || Infinity) < window.coyote_time) {
        player.velocity = [
            window.walljump_horiontal_power * player.wallslide, // Multiply by wallslide to get the direction
            -window.walljump_power
        ];
        player.in_jump = true;
        player.last_walljump = performance.now();
        player.last_walljump_key = player.wallslide;
        controller.last_jump_time = undefined
    }
    if (!controller.jump_button && player.velocity[1] < 0) {
        player.velocity[1] /= (1 + window.stop_jump_drag * delta_t);
    }
}

function update_player(player) {
    // If player next to a wall. Set wallslide to true. And cap the vertical velocity at maximum 10
    const time_since_ground_touch = performance.now() - player.last_ground_touch;
    if (get_block([player.location[0] - 0.43, player.location[1]]) !== 0 && time_since_ground_touch > 100) {
        player.wallslide = 1;
    } else if (get_block([player.location[0] + 0.42, player.location[1]]) !== 0 && time_since_ground_touch > 100) {
        player.wallslide = -1;
    } else {
        player.wallslide = false;
    }
    if (player.wallslide) {
        player.velocity[1] = Math.min(player.velocity[1], 8);
    }

    const new_update_time = performance.now();
    const delta_t = Math.min((new_update_time - player.last_updated) / 1000, 0.2) || 0.0;
    player.last_updated = new_update_time;
    const change_size = player.velocity.map((vel) => vel * delta_t);
    const amount_parts = Math.ceil(Math.max(...change_size.map(Math.abs)));

    player.velocity[0] = Math.min(Math.max(player.velocity[0], -60), 60);
    player.velocity[1] = Math.min(Math.max(player.velocity[1], -60), 60)

    for (let i = 0; i < amount_parts; i++) {
        const target_location = player.location.map((_, dim) => player.location[dim] + change_size[dim] / amount_parts);

        //X
        if (
            get_block([target_location[0] + 0.41 * neg_pos(player.velocity[0]), player.location[1] - 0.79]) !== 0 ||
            get_block([target_location[0] + 0.41 * neg_pos(player.velocity[0]), player.location[1]]) !== 0 ||
            get_block([target_location[0] + 0.41 * neg_pos(player.velocity[0]), player.location[1] + 0.79]) !== 0
        ) {
            if (player.velocity[0] > 0) {
                player.location[0] = Math.floor(target_location[0] + 0.41) - 0.42;
            } else {
                player.location[0] = Math.ceil(target_location[0] - 0.41) + 0.42;
            }
            //player.velocity[0] *= -0.1;
            player.velocity[0] = 0;
            player.last_wall_touch = performance.now();
            //my_player.in_jump = false;
        } else {
            player.location[0] = target_location[0];
        }
        //Y
        if (
            get_block([player.location[0] - 0.39, target_location[1] + 0.81 * neg_pos(player.velocity[1])]) !== 0 ||
            get_block([player.location[0] + 0.39, target_location[1] + 0.81 * neg_pos(player.velocity[1])]) !== 0
        ) {
            if (player.velocity[1] > 0) {
                player.location[1] = Math.floor(target_location[1] + 0.81) - 0.82;
                player.last_ground_touch = performance.now();
                player.in_jump = false;
            } else {
                player.location[1] = Math.ceil(target_location[1] - 0.81) + 0.82;
            }
            //player.velocity[1] *= -0.1;
            player.velocity[1] = 0;
            // while (
            //     get_block([player.location[0]-0.39, player.location[1]+ 0.81]) === 1 ||
            //     get_block([player.location[0]+0.39, player.location[1]+ 0.81]) === 1
            //     ) {
            //     player.location[1] -= 0.01;
            // }

        } else {
            player.location[1] = target_location[1];
        }

    }

    player.velocity[1] += window.gravity * delta_t; //gravity
    player.velocity[0] /= (1 + window.friction * 50 * delta_t);  //friction
    //if (player.velocity[1] > 0) { player.velocity[1] /= (1 + window.friction * delta_t) }
    player.delta_t = delta_t;
}