import { float_location_to_chunk_location, cast_ray, random_from_two_inputs } from "./math_utils.js";
import { get_block, chunk_size } from "./world.js";

// Local functions //

/**
 * Flips an image horizontally.
 * 
 * @param {string} src - The source URL of the image.
 * @param {HTMLImageElement} target - The target image element to apply the flipped image.
 */
function flip(src,target){
    var img = new Image();
    img.onload = function(){
        var c = document.createElement('canvas');
        c.width = this.width;
        c.height = this.height;
        var ctx = c.getContext('2d');
        ctx.scale(-1,1);
        ctx.drawImage(this,-this.width,0);
        this.onload = undefined;
        target.src = c.toDataURL();
    }
    img.src = src;
}

function float_to_screen(float_location, camera) {
    return [
        Math.round((float_location[0]-camera.location[0])*camera.block_size+camera.middle_offset[0]),
        Math.round((float_location[1]-camera.location[1])*camera.block_size+camera.middle_offset[1])
    ]
}

// Local variables //

let brick_texture = new Image();
brick_texture.src = 'textures/brick.png';
let gnome_left = new Image();
gnome_left.src = 'textures/gnome.png';
let gnome_right = new Image();
flip(gnome_left.src, gnome_right);
let gnome_slide_left = new Image();
gnome_slide_left.src = 'textures/gnome_wallslide.png';
let gnome_slide_right = new Image();
flip(gnome_slide_left.src, gnome_slide_right);

// Load tile textures
let tile_textures = {}
for (let block_number of [1]) {
    let img = new Image();
    img.src = `textures/tile${block_number}.png`;
    const tile_size = 8;
    tile_textures[block_number] = [];
    img.onload = () => {
        const n_tiles = img.width / tile_size;
        for (let i = 0; i < n_tiles; i++) {
            let canvas = new OffscreenCanvas(tile_size, tile_size);
            let ctx = canvas.getContext('2d');
            ctx.imageSmoothingEnabled = false;
            ctx.drawImage(img, i*tile_size, 0, tile_size, tile_size, 0, 0, tile_size, tile_size);
            tile_textures[block_number].push(canvas.transferToImageBitmap());
        }
    }
}

// Public variables //

let chunk_texture_cache = {}; // {chunk_location: Image}

// Public functions //

/**
 * Resets the chunk texture cache. If chunk_key is undefined then it resets the whole cache. Otherwise, it resets the cache for the given chunk.
 * @param {string} chunk_key - The key of the chunk to reset the cache for.
 */
export function reset_chunk_texture_cache(chunk_key=undefined) {
    if (chunk_key === undefined) {
        chunk_texture_cache = {};
    } else {
        chunk_texture_cache[chunk_key] = undefined;
    }
}

function draw_chunk(chunk_x, chunk_y, world, chunk_size, camera) {
    const full_blocks = [1];

    const chunk = world[JSON.stringify([chunk_x, chunk_y])];
    const chunk_canvas = new OffscreenCanvas(chunk_size*camera.block_size+1, chunk_size*camera.block_size+1);
    const chunk_ctx = chunk_canvas.getContext('2d');
    chunk_ctx.imageSmoothingEnabled = false;
    for (let block_y = 0; block_y < chunk.length; block_y++) {
        for (let block_x = 0; block_x < chunk[0].length; block_x++) {
            let block_texture = undefined;
            if (full_blocks.includes(chunk[block_y][block_x])) {
                const abs_block_x = chunk_x*chunk_size+block_x;
                const abs_block_y = chunk_y*chunk_size+block_y;
                let block_surroundings =  // NESW
                    8 * full_blocks.includes(get_block([abs_block_x, abs_block_y-1], world)) +
                    4 * full_blocks.includes(get_block([abs_block_x+1, abs_block_y], world)) +
                    2 * full_blocks.includes(get_block([abs_block_x, abs_block_y+1], world)) +
                    1 * full_blocks.includes(get_block([abs_block_x-1, abs_block_y], world));
                if (block_surroundings === 15) {
                    // pseudo random chance to draw side 16, 17, 18, or 19 based on the block location.
                    const random_number = random_from_two_inputs(abs_block_x, abs_block_y) % 25;
                    if (random_number <= 4) {
                        block_surroundings += random_number;
                    }
                }
                block_texture = tile_textures[chunk[block_y][block_x]][block_surroundings];
                //block_texture = brick_texture;
            } else if (chunk[block_y][block_x] === 2) {
                chunk_ctx.fillStyle = "#EECC00"
                chunk_ctx.fillRect(block_x*camera.block_size, block_y*camera.block_size,
                    camera.block_size+1, camera.block_size+1);
            }

            if (block_texture !== undefined) {
                chunk_ctx.drawImage(block_texture,
                    block_x * camera.block_size, block_y * camera.block_size,
                    camera.block_size + 1, camera.block_size + 1
                )
            }

            if (window.debug && camera.block_size >= 50) {
                chunk_ctx.font=`${camera.block_size/5}px PixelArt`;
                chunk_ctx.fillStyle = 'red';
                chunk_ctx.fillText(`(${chunk_x*chunk_size+block_x}, ${chunk_y*chunk_size+block_y})`,
                    block_x*camera.block_size, block_y*camera.block_size+camera.block_size*0.5);
            }
        }
    }
    return chunk_canvas.transferToImageBitmap();
}

export function draw_world(g_ctx, camera, world, chunk_size) {
    g_ctx.imageSmoothingEnabled = false;
    const start_chunk_location = float_location_to_chunk_location(camera.location.map((_, dim) =>
        (camera.location[dim]*camera.block_size-camera.middle_offset[dim])/camera.block_size), chunk_size);
    const end_chunk_location = float_location_to_chunk_location(camera.location.map((_, dim) =>
        (camera.location[dim]*camera.block_size+camera.middle_offset[dim])/camera.block_size), chunk_size);
    //const start_chunk_location = [-1, -1];
    //const end_chunk_location = [-1, -1];

    for (let chunk_y=start_chunk_location[1]; chunk_y<=end_chunk_location[1]; chunk_y++) {
        for (let chunk_x=start_chunk_location[0]; chunk_x<=end_chunk_location[0]; chunk_x++) {
            const chunk_key = JSON.stringify([chunk_x, chunk_y]);
            // If the chuck is not in the texture cache, then render the chunk and add it to the texture cache.
            if (chunk_texture_cache[chunk_key] === undefined) {
                if (world[chunk_key] === undefined) {continue;}
                chunk_texture_cache[chunk_key] = draw_chunk(chunk_x, chunk_y, world, chunk_size, camera);
            }
            
            // In any case after that, we'll update the position in the texture cache. And render the chunk to the canvas.
            g_ctx.drawImage(chunk_texture_cache[chunk_key], 
                (chunk_x*chunk_size-camera.location[0])*camera.block_size+camera.middle_offset[0],
                (chunk_y*chunk_size-camera.location[1])*camera.block_size+camera.middle_offset[1]);
            
            if (window.debug) {
                g_ctx.strokeStyle = 'red';
                g_ctx.lineWidth = 1;
                g_ctx.strokeRect(
                    (chunk_x*chunk_size-camera.location[0])*camera.block_size+camera.middle_offset[0],
                    (chunk_y*chunk_size-camera.location[1])*camera.block_size+camera.middle_offset[1],
                    chunk_size*camera.block_size, chunk_size*camera.block_size
                );
            }
        }
    }
}

export function draw_players(g_ctx, camera, players, my_id, controller) {
    for (let [player_id, player] of Object.entries(players)) {
        let gnome_texture;
        if (player.wallslide) {
            if (player.wallslide > 0) {
                gnome_texture = gnome_slide_right;
            } else {
                gnome_texture = gnome_slide_left;
            }
        } else {
            if (player.velocity[0] >= 0) {
                gnome_texture = gnome_right;
            } else {
                gnome_texture = gnome_left
            }
        }
        g_ctx.drawImage(gnome_texture,
            (player.location[0]-camera.location[0])*camera.block_size+camera.middle_offset[0] - camera.block_size*0.4,
            (player.location[1]-camera.location[1])*camera.block_size+camera.middle_offset[1] - camera.block_size*0.8,
            camera.block_size*0.8, camera.block_size*1.6
        );

        if (window.debug) {
            g_ctx.strokeStyle = 'red';
            g_ctx.lineWidth = 1;
            g_ctx.strokeRect(
                (player.location[0]-camera.location[0])*camera.block_size+camera.middle_offset[0] - camera.block_size*0.4,
                (player.location[1]-camera.location[1])*camera.block_size+camera.middle_offset[1] - camera.block_size*0.8,
                camera.block_size*0.8, camera.block_size*1.6);
        }
    }
}

export function draw_aim(g_ctx, camera, player, controller) {
    const direction = controller.aim_direction;
    // If direction is NaN then there is no aiming to draw.
    if (isNaN(direction[0]) || isNaN(direction[1])) {
        return;
    }
    const angle = Math.atan2(direction[1], direction[0]) - Math.PI*0.5;
    g_ctx.lineWidth = 2;
    g_ctx.beginPath();
    g_ctx.strokeStyle = '#FFFFFF';
    g_ctx.arc(...float_to_screen(player.location.map((loc, dim) => loc+direction[dim]*1.5), camera),
        camera.block_size*0.1, angle, angle+Math.PI);
    g_ctx.moveTo(...float_to_screen(player.location.map((loc, dim) => loc + direction[dim] * 1.5), camera));
    g_ctx.lineTo(...float_to_screen(player.location.map((loc, dim) => loc+direction[dim]*1.75), camera));

    g_ctx.stroke();
    const cast_output = cast_ray(player.location, controller.aim_direction);
    if (cast_output !== undefined) {
        const block_location = cast_output[cast_output.length-1];
        g_ctx.fillStyle = 'rgba(225,225,225,0.5)';
        g_ctx.fillRect(...float_to_screen(block_location.map(Math.floor), camera), camera.block_size, camera.block_size);

        if (window.debug) {
            cast_output.map(val => val.map(Math.floor)).forEach((block_location => { 
                g_ctx.fillRect(...float_to_screen(block_location, camera), camera.block_size, camera.block_size); 
            }))
            // Draw a circle on the exact point where the ray hits the block.
            for (let i = 1; i < cast_output.length; i++) {
                g_ctx.beginPath();
                g_ctx.fillStyle = '#FFFFFF';
                g_ctx.arc(...float_to_screen(cast_output[i], camera), camera.block_size*0.05, 0, Math.PI*2);
                g_ctx.fill();
            }
        }
    }
}

export function draw_delta_t_history(g_c, g_ctx, delta_t_history) {
    // draw a graph in the upper right corner of the screen.
    g_ctx.strokeStyle = '#FFFFFF';
    g_ctx.beginPath();
    g_ctx.moveTo(g_c.width-1, 0);
    for (let [index, delta_t] of delta_t_history.entries()) {
        g_ctx.lineTo(g_c.width-1-index*2, g_c.height-delta_t*2);
    }
    g_ctx.stroke();
    // draw a line at 16.67 ms (60 fps) and 33.33 ms (30 fps)
    g_ctx.strokeStyle = '#00FF00';
    for (let fps of [30, 60]) {
        const looptime = 1000 / fps;
        g_ctx.beginPath();
        g_ctx.moveTo(g_c.width - 1, g_c.height - looptime * 2);
        g_ctx.lineTo(g_c.width - 1 - delta_t_history.length * 2, g_c.height - looptime * 2);
        g_ctx.stroke();
    }
}

export function draw_cursor_coordinate(g_ctx, camera, controller) {
    // Get mouse location
    let mouse_cursor_location = controller.mouse_position;
    //draw cross
    g_ctx.strokeStyle = 'blue';
    g_ctx.lineWidth = 1;
    g_ctx.beginPath();
    g_ctx.moveTo(mouse_cursor_location[0] - 10, mouse_cursor_location[1]);
    g_ctx.lineTo(mouse_cursor_location[0] + 10, mouse_cursor_location[1]);
    g_ctx.moveTo(mouse_cursor_location[0], mouse_cursor_location[1] - 10);
    g_ctx.lineTo(mouse_cursor_location[0], mouse_cursor_location[1] + 10);
    g_ctx.stroke();
    // font: fonts/slkscr.ttf
    g_ctx.font = '16px PixelArt';
    g_ctx.fillStyle = 'blue';
    let cursor_location = mouse_cursor_location.map((loc, dim) => ((loc - camera.middle_offset[dim]) / camera.block_size + camera.location[dim]).toFixed(2));
    g_ctx.fillText(`F(${cursor_location[0]}, ${cursor_location[1]})`, mouse_cursor_location[0]+10, mouse_cursor_location[1]-10);
    let block_location = cursor_location.map(Math.floor);
    g_ctx.fillText(`B(${block_location[0]}, ${block_location[1]})`, mouse_cursor_location[0]+10, mouse_cursor_location[1]+10);
    let chunk_location = float_location_to_chunk_location(cursor_location, chunk_size);
    g_ctx.fillText(`C(${chunk_location[0]}, ${chunk_location[1]})`, mouse_cursor_location[0]+10, mouse_cursor_location[1]+30);
}