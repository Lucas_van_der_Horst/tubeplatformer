import { draw_world, draw_players, draw_aim, draw_delta_t_history, reset_chunk_texture_cache, draw_cursor_coordinate } from "./drawing.js";
import { neg_pos, normalize_vector, float_location_to_chunk_location, cast_ray } from "./math_utils.js";
import { get_block, set_block, world, chunk_size } from "./world.js";
import { controller } from "./controller.js";
import {} from "./UI.js";
import { physics_step} from "./physics.js";

export let players = {}; // {player_id: {location: [x, y], velocity: [x, y], last_updated: performance.now()}}
export let camera = {
    location: [0, 0],
    block_size: 60,
    middle_offset: [0,0],
    follow_percentage_speed: 1.0
}
window.camera = camera;

let socket = io();
let g_c = document.getElementById("game_canvas");
let g_ctx = g_c.getContext('2d');
window.debug = false;
let chunks_requested = [];
export let min_block_size = 25;
export let max_block_size = 80;
let cam_follow_player_id;
function size_canvas() {
    g_c.width = window.innerWidth/1;
    g_c.height = window.innerHeight/1;
    camera.middle_offset = [g_c.width/2, g_c.height/2];
}
size_canvas();
addEventListener('resize', size_canvas);
const load_size = [7, 4];
let my_id;
let delta_t_history = [];
window.physics_delay = 16;

export function shoot_block() {
    if (my_id === undefined) { return }
    const cast_output = cast_ray(players[my_id].location, controller.aim_direction);
    if (cast_output.length > 2) {
        //cast_output.forEach((item) => {set_block(item, 2);})d
        send_set_block(cast_output[cast_output.length-2], 1)
    }
}

export function break_block() {
    if (my_id === undefined) {return}
    const cast_output = cast_ray(players[my_id].location, controller.aim_direction);
    if (cast_output.length > 1) {
        //cast_output.forEach((item) => {set_block(item, 2);})d
        send_set_block(cast_output[cast_output.length-1], 0)
    }
}


function game_loop() {
    let start_time = performance.now();
    g_ctx.clearRect(0, 0, g_c.width, g_c.height);

    let my_player = players[my_id];
    // Go to a percentage of the way to the player location.
    // TODO: should use delta_t somewhere here.
    camera.location = camera.location.map((loc, dim) => loc + (players[cam_follow_player_id].location[dim]-loc)*camera.follow_percentage_speed);

    controller.aim_direction = normalize_vector(controller.aim_direction);
    draw_world(g_ctx, camera, world, chunk_size);
    draw_players(g_ctx, camera, players, my_id, controller);
    draw_aim(g_ctx, camera, my_player, controller);
    delta_t_history.push(performance.now() - start_time);
    if (delta_t_history.length > 50) {delta_t_history.shift();}
    if (window.debug) {
        draw_delta_t_history(g_c, g_ctx, delta_t_history);
        draw_cursor_coordinate(g_ctx, camera, controller);
    }
    window.requestAnimationFrame(game_loop);
}

socket.on('set_user_id', (user_id) => {
    if (my_id !== undefined) {
        document.getElementById("reload_popup").style.display = "block";
    } else {
        my_id = user_id;
        cam_follow_player_id = my_id
        players[my_id].last_send_location = [...players[my_id].location];

        // Physics loop
        //setInterval(physics_step, 16);
        let last_physics_update = performance.now();
        (function loop() {
            const physics_now = performance.now();
            const delta_t = Math.min((physics_now - last_physics_update), 0.2) || 0.0;
            physics_step(delta_t, players, controller, socket, my_id);
            last_physics_update = physics_now;
            setTimeout(loop, window.physics_delay);
        })();

        game_loop();
    }
});

socket.on('update_players', (new_players) => {
    // We probably have a newer version of my player, than the server, so we ignore the server's version.
    let my_player = players[my_id];
    players = Object.assign(players, new_players);
    // Restore my player
    if (my_player !== undefined) {
        players[my_id] = my_player;
    }
});

socket.on('set_players', (new_players) => {
    players = new_players;
});

socket.on('remove_player', player_id => {
    delete players[player_id];
});

socket.on('set_chunks', chunks_zipped => {
    chunks_zipped.forEach(zip => {
        const chunk_location = zip[0];
        world[JSON.stringify(chunk_location)] = zip[1];
        // If the surrounding chunks have cached textures then clear them.
        for (let dy=-1; dy<=1; dy++) {
            for (let dx=-1; dx<=1; dx++) {
                reset_chunk_texture_cache(JSON.stringify([chunk_location[0]+dx, chunk_location[1]+dy]));
            }
        }
    })
})


// Request chunks around the player
setInterval(() => {
    const middle_chunk_location = float_location_to_chunk_location([
        camera.location[0], camera.location[1]
    ], chunk_size);
    let chunks = [];
    for (let y=-load_size[1]; y<=load_size[1]; y++) {
        for (let x=-load_size[0]; x<=load_size[0]; x++) {
            const check_chunk_location = [middle_chunk_location[0]+x, middle_chunk_location[1]+y];
            const chunk_location_string = JSON.stringify(check_chunk_location);
            if (!(chunks_requested.includes(chunk_location_string))) {
                chunks_requested.push(chunk_location_string);
                chunks.push(check_chunk_location);
            }
        }
    }
    if (chunks.length > 0) {
        socket.emit('get_chunks', chunks);
    }
}, 100);

function send_set_block(block_location, block) {
    set_block(block_location, block);
    socket.emit('set_block', [block_location, block]);
}

socket.on('set_block', data => {
    set_block(data[0], data[1]);
});
