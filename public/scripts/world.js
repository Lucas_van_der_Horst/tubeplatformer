import {float_location_to_chunk_location, mod } from "./math_utils.js";
import {reset_chunk_texture_cache } from "./drawing.js";

export let world = {}; // {chunk_location: [[block, block, ...], [block, block, ...], ...]}
export const chunk_size = 8;

export function get_block(location) {
    location = location.map(Math.floor);
    let chunk = world[JSON.stringify(float_location_to_chunk_location(location, chunk_size))];
    if (chunk === undefined) {
        return undefined;
    } else {
        return chunk[mod(location[1], chunk_size)][mod(location[0], chunk_size)];
    }
}
window.get_block = get_block;

export function set_block(block_location, block) {
    block_location = block_location.map(Math.floor);
    const chunk_key = JSON.stringify(float_location_to_chunk_location(block_location, chunk_size));
    let chunk = world[chunk_key];
    if (chunk === undefined) {
        return undefined;
    }
    // Update chunk data
    chunk[mod(block_location[1], chunk_size)][mod(block_location[0], chunk_size)] = block;
    // clear the chunk texture cache
    reset_chunk_texture_cache(chunk_key);
    // If the block is on the edge of the chunk then update the neighboring chunk
    if (mod(block_location[0], chunk_size) === 0) {
        reset_chunk_texture_cache(JSON.stringify([float_location_to_chunk_location(block_location, chunk_size)[0]-1, float_location_to_chunk_location(block_location, chunk_size)[1]]));
    }
    if (mod(block_location[1], chunk_size) === 0) {
        reset_chunk_texture_cache(JSON.stringify([float_location_to_chunk_location(block_location, chunk_size)[0], float_location_to_chunk_location(block_location, chunk_size)[1]-1]));
    }
    if (mod(block_location[0], chunk_size) === chunk_size-1) {
        reset_chunk_texture_cache(JSON.stringify([float_location_to_chunk_location(block_location, chunk_size)[0]+1, float_location_to_chunk_location(block_location, chunk_size)[1]]));
    }
    if (mod(block_location[1], chunk_size) === chunk_size-1) {
        reset_chunk_texture_cache(JSON.stringify([float_location_to_chunk_location(block_location, chunk_size)[0], float_location_to_chunk_location(block_location, chunk_size)[1]+1]));
    }
}
window.set_block = set_block;