import { get_block } from "./world.js";

/**
 * Returns -1 if the number is negative, 1 if the number is positive, and 1 if the number is zero.
 *
 * @param {number} num - The number to check.
 * @returns {number} -1 if the number is negative, 1 if the number is positive, and 1 if the number is zero.
 */
export const neg_pos = (num) => {
    if (num < 0) {
        return -1;
    } else if (num >= 0) {
        return 1;
    }
};

/**
 * Normalizes a vector.
 * @param {number[]} vector - The vector to be normalized.
 * @returns {number[]} - The normalized vector.
 */
export function normalize_vector(vector) {
    const len = Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2))
    return vector.map((val, _) => val / len);
}

export function float_location_to_chunk_location(float_location, chunk_size) {
    return float_location.map((_, dim) => Math.floor(float_location[dim]/chunk_size));
}

/**
 * A normal modulo function. But if the number is exactly zero then it returns the divisor.
 * @returns {number} The modulus of num and mod.
 */
 function ceil_mod(num, div) {
    // Because the default modulo is defined on negative numbers so it returns a negative number, we change that bit too. As to always return a positive number. So mod(-0.2, 1) == 0.8 instead of -0.2.
    const normal_mod = ((num % div) + div) % div;
    return normal_mod === 0 ? div : normal_mod;
}

/**
 * The opposite of ceil_mod. If the number is exactly the divisor then it returns zero.
 * The default modulo operator does that too. But does output negative numbers if the number is negative.
 * This function always returns a positive number.
 * @returns 
 */
function floor_mod(num, div) {
    return ((num % div) + div) % div;
}

/**
 * Casts a ray from a start point in a direction and returns the block locations that the ray passes through until it hits a block.
 * And returns all the block locations that the ray passes through including the block that it hits.
 * If the ray doesn't hit a block then it returns undefined.
 * @param {number[]} start_point - The starting point of the ray.
 * @param {number[]} direction - The direction of the ray.
 * @returns {number[][]} The block locations that the ray passes through.
 */
export function cast_ray(start_point, direction) {
    // Have a working point that starts at the start point and takes a step calculated by the distance to the next block in the direction of the ray.
    // The tile coordinate is simply the floor of the float coordinate.
    let working_point = [...start_point];
    let blocks_passed = [];
    while (true) {
        // If the euclidean distance between the working point and the start point is greater than the max distance then return undefined.
        if (euclidean_distance_squared(working_point, start_point) > 100*100) {
            return undefined;
        }
        let working_block = [
            // direction[0] < 0 ? Math.floor(working_point[0]) : Math.ceil(working_point[0]),
            // direction[1] < 0 ? Math.floor(working_point[1]) : Math.ceil(working_point[1])
            Math.floor(working_point[0]),
            Math.floor(working_point[1])
        ];
        // Add the working point to the blocks passed.
        blocks_passed.push([...working_point]);
        // If the block at the working point is not air then return the blocks passed.
        if (get_block(working_block) !== 0) {
            return blocks_passed;
        }

        // Calculate and compare the stepsizes to the next block in the x and y directions.
        const horizontal_stepsize = (direction[0] > 0 ? 1-floor_mod(working_point[0], 1) : ceil_mod(working_point[0], 1)) / Math.abs(direction[0]);
        const vertical_stepsize = (direction[1] > 0 ? 1-floor_mod(working_point[1], 1) : ceil_mod(working_point[1], 1)) / Math.abs(direction[1]);
        // Take a step in the direction of the smallest stepsize.
        const stepsize = Math.min(horizontal_stepsize, vertical_stepsize);
        working_point[0] += direction[0] * stepsize;
        working_point[1] += direction[1] * stepsize;
    }
}

const random_list = [469, 936, 125, 842, 357, 528, 821, 37, 523, 455, 885, 998, 960, 179, 584, 910, 993, 952, 482, 270, 14, 100, 200, 605, 216, 66, 533, 441, 423, 630, 416, 999, 47, 860, 446, 395, 825, 513, 971, 270, 510, 520, 804, 354, 169, 845, 952, 872, 526, 952, 193, 522, 228, 768, 171, 549, 106, 908, 299, 148, 112, 792, 332, 907, 942, 765, 118, 542, 968, 133, 238, 927, 797, 949, 690, 172, 430, 180, 154, 99, 535, 487, 993, 601, 821, 584, 241, 101, 325, 101, 965, 101, 801, 449, 607, 204, 117, 967, 41, 902, 254, 378, 22, 75, 62, 341, 638, 728, 370, 425, 846, 485, 114, 130, 517, 846, 474, 330, 159, 287, 127, 375, 409, 433, 678, 528, 952, 39, 618, 377, 455, 28, 172, 891, 204, 400, 127, 621, 120, 17, 606, 871, 116, 581, 519, 985, 20, 130, 287, 179, 89, 712, 55, 674, 684, 330, 556, 22, 260, 606, 70, 722, 190, 186, 158, 471, 5, 337, 644, 0, 958, 848, 186, 578, 878, 159, 162, 659, 7, 185, 895, 447, 832, 357, 741, 1, 778, 121, 650, 704, 10, 53, 539, 206, 87, 786, 433, 587, 886, 85, 347, 799, 479, 22, 462, 847, 500, 758, 111, 797, 442, 570, 801, 887, 403, 294, 601, 295, 44, 608, 553, 576, 864, 378, 640, 238, 924, 771, 288, 534, 238, 540, 902, 556, 861, 916, 784, 751, 525, 853, 910, 139, 393, 843, 366, 50, 147, 15, 201, 962, 125, 626, 495, 431, 44, 309, 183, 177, 175, 568, 956, 527, 544, 306, 162, 454, 978, 401, 742, 162, 342, 321, 364, 362, 593, 264, 808, 677, 710, 765, 671, 132, 471, 463, 920, 873, 355, 670, 117, 715, 560, 978, 99, 169, 684, 666, 982, 787, 728, 177, 915, 306, 859, 565, 805, 645, 417, 582, 108, 10, 959, 535, 761, 378, 47, 207, 600, 788, 267, 862, 45, 89, 602, 808, 345, 412, 933, 225, 804, 771, 182, 700, 731, 846, 59, 788, 160, 135, 3, 112, 987, 198, 59, 871, 157, 758, 159, 990, 80, 643, 45, 215, 222, 711, 233, 880, 130, 396, 348, 519, 206, 774, 451, 96, 179, 469, 682, 204, 353, 737, 70, 819, 325, 740, 266, 473, 576, 451, 880, 281, 825, 445, 945, 245, 956, 178, 560, 776, 171, 987, 95, 401, 886, 144, 197, 862, 284, 531, 934, 649, 583, 504, 412, 777, 600, 728, 166, 768, 103, 399, 131, 976, 969, 400, 139, 448, 507, 436, 77, 352, 703, 188, 428, 504, 932, 935, 406, 111, 671, 256, 801, 233, 525, 450, 803, 311, 325, 91, 404, 847, 633, 828, 508, 550, 415, 126, 929, 502, 721, 685, 925, 725, 93, 755, 677, 23, 212, 743, 782, 151, 540, 967, 688, 859, 71, 962, 781, 305, 996, 4, 244, 928, 564, 36, 657, 643, 22, 670, 102, 563, 768, 951, 226, 273, 503, 594, 709, 287, 785, 469, 295, 723, 792, 733, 575, 887, 616, 234, 371, 68];

/**
 * The normal modulus works weird with negative numbers. This function fixes that.
 * @param {number} n - The dividend.
 * @param {number} m - The divisor.
 * @returns {number} The modulus of n and m.
 */
export function mod(n, m) {
    const remain = n % m;
    return Math.floor(remain >= 0 ? remain : remain + m);
}

export function random_from_two_inputs(input1, input2) {
    return random_list[(random_list[mod(input1 * 2, 500)] + random_list[mod(input2 * 2, 500)]) % 500];
}

export function euclidean_distance_squared(point1, point2) {
    return Math.pow(point1[0] - point2[0], 2) + Math.pow(point1[1] - point2[1], 2);
}