import { normalize_vector } from './math_utils.js';
import { shoot_block, break_block, camera, min_block_size, max_block_size } from './main.js';
import { reset_chunk_texture_cache } from './drawing.js';
import { showing_help } from './UI.js';

export let controller = {
    horizontal_move: 0,
    key_a: false,
    key_d: false,
    jump_button: false,  // Whether or not the jump button is pressed at this moment
    last_jump_time: performance.now(),  // The last time the jump button was pressed when it wasn't before.
    mouse_position: [0, 0],
    aim_direction: [0, 0],
    place_block_button: false,
    remove_block_button: false
}

function update_horizontal () {
    controller.horizontal_move = 1*(controller.key_d) + -1*(controller.key_a);
}

window.addEventListener('gamepadconnected', (_event) => {
    console.log("connected gamepad")
    const update = () => {
        for (const gamepad of navigator.getGamepads()) {
            if (!gamepad) continue;
            for (const [index, axis] of gamepad.axes.entries()) {
                if(index===0) {controller.horizontal_move = axis}
                if(index===2) {controller.aim_direction[0] = axis}
                if(index===3) {controller.aim_direction[1] = axis}
            }
            if (gamepad.buttons[0].pressed || gamepad.buttons[11].pressed || gamepad.buttons[7].pressed) {
                if (!controller.jump_button) {
                    controller.last_jump_time = performance.now();
                }
                controller.jump_button = true;
            } else {
                controller.jump_button = false;
            }
            if (gamepad.buttons[5].pressed && !controller.place_block_button) {shoot_block()}
            controller.place_block_button = gamepad.buttons[5].pressed;
            if (gamepad.buttons[4].pressed && !controller.remove_block_button) {break_block()}
            controller.remove_block_button = gamepad.buttons[4].pressed;
        }
        requestAnimationFrame(update);
    };
    update();
});

document.addEventListener('keydown', (event) => {
    if (showing_help) {return}
    if (event.key === 'a' || event.key === 'ArrowLeft') {controller.key_a = true; update_horizontal()}
    if (event.key === 'd' || event.key === 'ArrowRight') {controller.key_d = true; update_horizontal()}
    if (event.key === ' ' || event.key === 'w' || event.key === 'ArrowUp') {
        if (!controller.jump_button) {
            controller.last_jump_time = performance.now();
        }
        controller.jump_button = true;
    }
});

document.addEventListener('keyup', (event) => {
    if (event.key === 'a' || event.key === 'ArrowLeft') {controller.key_a = false; update_horizontal()}
    if (event.key === 'd' || event.key === 'ArrowRight') {controller.key_d = false; update_horizontal()}
    if (event.key === ' ' || event.key === 'w' || event.key === 'ArrowUp') {controller.jump_button=false}
});

window.addEventListener('wheel', (evt) => {
    camera.block_size = Math.min(Math.max(camera.block_size - evt.deltaY/50, min_block_size), max_block_size)
    // as the chunk texture cache is based on the camera block size, we need to clear it.
    reset_chunk_texture_cache();
});

window.addEventListener('mousemove', e => {
    controller.mouse_position = [e.offsetX, e.offsetY];
    controller.aim_direction = normalize_vector([e.offsetX-window.innerWidth/2, e.offsetY-window.innerHeight/2]);
});

// Left click
window.addEventListener('click', () => {
    if (!showing_help) {
        break_block();
    }
});

// Right click
window.oncontextmenu = () => { shoot_block(); return false }; // cancel default menu