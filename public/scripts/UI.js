export let showing_help = true;

window.show_help = function show_help() {
    showing_help = true;
    document.getElementById("help_popup").style.display = "block";
    document.getElementById("help_button").style.display = "none";
}

window.hide_help = function hide_help() {
    showing_help = false;
    document.getElementById("help_popup").style.display = "none";
    document.getElementById("help_button").style.display = "block";
}

// Request /version and set it as version_text
fetch('/version').then(response => response.text()).then(version => {
    document.getElementById("version_text").innerText = `Version: ${version}`;
});
